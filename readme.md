
**Setup infrastructure on aws using terraform and ansible**
======================================================

## Install pip

>`apt install python-pip`

## Install ansible

>`pip install ansible`

## Install aws cli

>`pip install awscli --upgrade --user`

## configure ssh for ansible work correctly:
add to ~/.ssh/config:
```
Host *
   StrictHostKeyChecking no
   UserKnownHostsFile=/dev/null
```
---
## Install terraform
```
wget https://releases.hashicorp.com/terraform/0.12.2/terraform_0.12.2_linux_amd64.zip && unzip terraform_0.12.2_linux_amd64.zip -d /bin/terraform
```
---
## Terraform init to download dependencies
```
terraform init
```
## change config:
you should change the aws credential by issue export command:
```
export AWS_ACCESS_KEY_ID=AKIA4NIJXSE********`
export AWS_SECRET_ACCESS_KEY=CTazkiQP7C2PKFKYGDcf********
```
change the aws region at in the terraform.tfvars:
>`aws_region        = "us-east-1"`

change your ssh publickey for remoting ssh to this server
>`public_key_path   = "/home/vagrant/.ssh/phultv.pub"`

Change you ssh privatekey for ansible remote to ec2 server
>`private_key_path = "/home/vagrant/.ssh/phultv.rsa"`
---
## Terraform check config
```
terraform plan
```
## Terraform deploy infrastructures to aws
```
terraform apply
```

##
```
curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/aws-iam-authenticator
chmod +x aws-iam-authenticator
mv aws-iam-authenticator /usr/local/bin/
```