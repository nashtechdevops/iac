#
# Variables Configuration
#

variable "cluster-name" {
  default = "eks-demo"
  type    = string
}

variable "aws_profile" {
  default = "default"
  type    = string
}

variable "vpc_id" {
}
variable "privatesubnet1" {
}
variable "privatesubnet2" {
}
variable "publicsubnet1" {
}
variable "publicsubnet2" {
}